
import EnergyA from "../resources/img/v.10/으랏차차요정.png"
import EnergyB from "../resources/img/v.10/으쌰으쌰요정.png"
import EcologyA from "../resources/img/v.10/두근두근요정.png"
import EcologyB from "../resources/img/v.10/몽글몽글요정.png"
import FoodA from "../resources/img/v.10/장바구니요정.png"
import FoodB from "../resources/img/v.10/오물오물요정.png"
import SavingA from "../resources/img/v.10/자린고비요정.png"
import SavingB from "../resources/img/v.10/알뜰살뜰요정.png"
import EnergyA_sub from "../resources/img/v.10/으랏차차요정_보조.png"
import EnergyB_sub from "../resources/img/v.10/으쌰으쌰요정_보조.png"
import EcologyA_sub from "../resources/img/v.10/두근두근요정_보조.png"
import EcologyB_sub from "../resources/img/v.10/몽글몽글요정_보조.png"
import FoodA_sub from "../resources/img/v.10/장바구니요정_보조.png"
import FoodB_sub from "../resources/img/v.10/오물오물요정_보조.png"
import SavingA_sub from "../resources/img/v.10/자린고비요정_보조.png"
import SavingB_sub from "../resources/img/v.10/알뜰살뜰요정_보조.png"


import emo2a from "../resources/img/v.10/emo2a.png"
import emo3a from "../resources/img/v.10/emo3a.png"
import emo4a from "../resources/img/v.10/emo4a.png"
import emo6a from "../resources/img/v.10/emo6a.png"
import emo6b from "../resources/img/v.10/emo6b.png"
import emo7a from "../resources/img/v.10/emo7a.png"
import emo11a from "../resources/img/v.10/emo11a.png"
import emo11b from "../resources/img/v.10/emo11b.png"


const FairyLists=[
    {   type: "Energy",
        type_k:"에너지",
        isActiveType:true,
        contentsA: "여름철 에어컨이 너무 세게 틀어진 가게에서 점원에게 세기를 줄여 달라고 부탁하는 당신! 사실 당신은 에너지 타입의 기후위기 요정이에요.",
        Fairys:[
        {
            name: "으랏차차 요정",
            isAvailable:false,
            hashtag: "짜잔, 저는 으랏차차 요정이래요! #절전 #탈석탄 #에너지전환 #숨은요정찾기 #티슈오피스 @tissueoffice 숨은 요정 찾기 👉 findyourfairy.com",
            contentsB: "으랏차차 요정이\n외우는 기후위기의 주문은,\n'에너지를 탈탈탈탈!'",
            contentsC: "한국의 온실가스 배출량은 OECD 회원국 중 이미 5위를 기록했는데도, 국내 석탄 화력 발전소는 무려 60기나 가동 중이고 심지어 7기는 새로 짓고 있어요. 재생 에너지 기술력이 충분해지면서 석탄 에너지가 사양 산업이라는 공감대는 이미 해외에서는 형성되고 있답니다.",
            contentsD: "으랏차차 요정의 주문을 외우면, 어디선가 대기업이 짓는 석탄 화력 발전소가 친환경이라는 말을 들어도 그것이 뻔한 거짓말임을 알 수 있게 되어요.",
            instagramLink: "",
            mainImage:EnergyA,
            subImage:EnergyA_sub ,
            emoj:emo11b,
        },
        {
            name: "으쌰으쌰 요정",
            isAvailable:true,
            
            hashtag: "#짜잔, 저는 으쌰으쌰 요정이래요!  으쌰으쌰 요정! #걷기 #비전화 #자가발전 #숨은요정찾기 #티슈오피스 @tissueoffice 숨은 요정 찾기 👉 findyourfairy.com",
            contentsB: "으쌰으쌰 요정이\n외우는 기후위기의 주문은,\n'절대지켜 에네르기!'",
            contentsC: "'비행기 여행의 부끄러움'이라는 뜻의 스웨덴의 신조어 '플뤼그스캄(Flygskam)' 운동을 아시나요? 탄소 배출량이 엄청난 탈것인 비행기를 타지 않는다는 운동이랍니다. 기후위기를 가속화하는 지금의 에너지 시스템은 이렇게 제도와 더불어 개인의 노력이 함께할 때 늦출 수 있어요.",
            contentsD: "으쌰으쌰 요정의 주문을 외우면 비행기보단 기차를, 택시보단 버스를, 버스보단 걷는 즐거움에 눈을 뜨게 되어요.",
            instagramLink: "https://www.instagram.com/ar/346856533251582/?ch=ODVjZTYyYjEzYzY0MTEzMTNlMTczZjUwN2EzM2M4ZTM%3D",
            mainImage:EnergyB,
            subImage:EnergyB_sub ,
            emoj:emo3a,
        },
        
        ]
    },
    {   type: "Ecology",
        type_k:"생태계",
        isActiveType:true,
        contentsA: "코로나19 판데믹, 전에 없던 긴 장마, 산불같은 재해 뉴스를 보며 혹시 이것이 인간 스스로 불러온 일이 아닐까 생각해보는 당신! 사실 당신은 생태계 타입의 기후위기 요정이에요.",
        Fairys:[
        {
            name: "두근두근 요정",
            isAvailable:true,
            hashtag: "짜잔, 저는 두근두근 요정이래요! #지역사회 #서식지보존 #도시숲 #숨은요정찾기 #티슈오피스 @tissueoffice 숨은 요정 찾기 👉 findyourfairy.com",
            contentsB: "두근두근 요정이 외우는\n기후위기의 주문은,\n'개발세발 막아막아!'",
            contentsC: "'그린 워싱(green washing)'을 아시나요? '위장환경주의'로 번역되는 이 단어는 '기업들이 실제로는 친환경 경영과는 거리가 있지만 녹색경영을 하는 것처럼 이미지 세탁하는 현상'을 가리켜요. 기업은 친환경이라는 이름 아래 유해 사업 및 제품을 정당화하여 마구 개발하곤 하죠. 지금은 골칫덩이인 플라스틱도 산업화 시대에는 획기적인 소재였답니다.",
            contentsD: "두근두근 요정의 주문을 외우면, 이런 개발의 함정에 빠지지 않기 위해 관련 정책이 들릴 때 귀를 기울일 수 있게 되어요.",
            instagramLink: "https://www.instagram.com/ar/618856405458872/?ch=MjRmMzUzYWM2NDNiZThkYmYwNzViYWQxMWQxMmUxZGI%3D",
            mainImage:EcologyA,
            subImage:EcologyA_sub ,
            emoj:emo2a,
        },
        {
            name: "몽글몽글 요정",
            isAvailable:false,
            hashtag: "짜잔, 저는 몽글몽글 요정이래요!  #우리동네 #해양온난화 #멸종위기종 #숨은요정찾기 #티슈오피스 @tissueoffice 숨은 요정 찾기 👉 findyourfairy.com",
            contentsB: "몽글몽글 요정이 외우는\n기후위기의 주문은,\n'너랑나랑 위아더원!'",
            contentsC: "숲보다 바다에서 더 많은 산소를 만들어 낸다는 사실 알고 있었나요? 그런데 바다가 기후위기로 늘어난 이산화 탄소를 흡수해 바닷속 용존 산소량이 감소하고 수온이 상승하면서, 바다 생물들이 살 수 없는 환경이 되어가고 있어요. 바다는 모두 이어져 있어서, 망가지는 해양 생태계는 어느 한 나라만의 문제가 아니랍니다.",
            contentsD: "몽글몽글 요정의 주문을 외우면, 내가 매일 지나치던 우리 동네 작은 생태계부터 되돌아볼 수 있게 되어요.",
            instagramLink:"",
            mainImage:EcologyB,
            subImage:EcologyB_sub ,
            emoj:emo7a,
        },
        
        ]
    },
    {   type:"Food",
    type_k:"식생활",
    isActiveType:true,
    contentsA:"못먹고 남긴 음식물을 보며 지구에게 왠지 모를 미안함을 느끼는 당신! 사실 당신은 식생활 타입의 기후위기 요정이에요.",
    Fairys:[
    {
        name: "장바구니 요정",
        isAvailable:true,
        hashtag: "짜잔, 저는 장바구니 요정이래요!  #채식 #비거니즘 #로컬푸드 #숨은요정찾기 #티슈오피스 @tissueoffice 숨은 요정 찾기 👉 findyourfairy.com",
        contentsB:"장바구니 요정이 외우는\n기후위기의 주문은,\n'채식채식 기후미식!'",
        contentsC:"'우리는 왜 개는 사랑하고 돼지는 먹고 소는 신을까?'라는 유명한 질문, 생각해 본 적 있나요? 비거니즘은 개인이 할 수 있는 가장 빠르고 효과적인 기후위기 대응 액션이랍니다. 같은 단백질 양에서 온실가스는 소, 양고기나 양식 새우에서 가장 많이 발생돼요.",
        contentsD: "장바구니 요정의 주문을 외우면, 일주일에 하루만이라도 고기를 피하는 채식 습관이나,푸드 마일리지(food mileage)가 적은 로컬 푸드 식단의 가치를 알게 되어요.",
        instagramLink:"https://www.instagram.com/ar/2712756359040744/?ch=OGMzMjNlYzQ2ZDIwNTc1OTc1Mjg3YmY5Njg5NDQ4ZjQ%3D",
        mainImage:FoodA,
        subImage:FoodA_sub ,
        emoj:emo6a,
    },
    {
        name: "오물오물 요정",
        isAvailable:true,
        hashtag: "짜잔, 저는 오물오물 요정이래요!  #음식물 #식량난 #소식 #숨은요정찾기 #티슈오피스 @tissueoffice 숨은 요정 찾기 👉 findyourfairy.com",
        contentsB: "오물오물 요정이 외우는\n기후위기의 주문은,\n'사라져라 음쓰음쓰!'",
        contentsC: "기록적인 장마와 폭염 이후, 마트에서 장을 볼 때 엄청나게 치솟은 채소값을 본 적이 있을 거예요. 기후위기에 가장 영향을 많이 받는 산업인 농가에서 그 고비를 뚜렷하게 직면하고 있어요. 기후위기는 곧 식량의 위기이고, 이로 인한 '식량난민'의 문제는 이미 가난한 나라에서부터 시작되었고 당연히 우리나라도 피해갈 수 없어요.",
        contentsD: "오물오물 요정의 주문을 외우면, 저녁 식사는 먹을 만큼만 준비하고 음식물 쓰레기를 줄여 나갈 수 있게 되어요.",
        instagramLink: "https://www.instagram.com/ar/3348062355308030/?ch=Mzg3MmYyYWQzMmZkNjFmYTBmYjI3ZDE2MGViNmFkZjk%3D",
        mainImage:FoodB,
        subImage:FoodB_sub ,
        emoj:emo4a,
    },
    
    ]
},
{   type: "Saving",
type_k:"자원절약",
isActiveType:false,
    contentsA: "편의점 점원이 봉투 필요하시냐고 물으면 괜찮다며 사양하고, 잘 안 쓰는 물건은 주변 친구들에게 쿨하게 나눠주는 당신! 사실 당신은 자원절약 타입의 기후위기 요정이에요.",
    Fairys:[
    {
        name: "자린고비 요정",
        isAvailable:false,
        hashtag: "짜잔, 저는 자린고비 요정이래요!  #과포장 #탈플라스틱 #폐기물 #숨은요정찾기 #티슈오피스 @tissueoffice 숨은 요정 찾기 👉 findyourfairy.com",
        contentsB: "자린고비 요정이 외우는\n기후위기의 주문은,\n'플라스틱 사라져즘!'",
        contentsC: "인간 쓰레기를 먹이로 착각해 뱃속이 폐기물로 꽉 찬 동물의 사진을 본 적이 있을 거예요. 우리의 식탁으로 다시 돌아오는 미세 플라스틱도 결국은 큰 플라스틱이 잘게 쪼개져 발생하기도 한답니다.",
        contentsD: "자린고비 요정의 주문을 외우면, 플라스틱과 스티로폼 포장재는 피하고, 알갱이가 들어간 치약이나 스크럽제는 쓰지 않는 습관이 생기게 되어요.",
        instagramLink: "",
        mainImage:SavingA,
        subImage:SavingA_sub ,
        emoj:emo6b,
    },
    {
        name: "알뜰살뜰 요정",
        isAvailable:false,
        hashtag: "짜잔, 저는 알뜰살뜰 요정이래요!  #무료나눔 #제로웨이스트 #재사용 #숨은요정찾기 #티슈오피스 @tissueoffice 숨은 요정 찾기 👉 findyourfairy.com",
        contentsB: "알뜰살뜰 요정이 외우는\n기후위기의 주문은,\n'용기내서 용기거절!'",
        contentsC: "'3R(Reduce, Reuse, Recyle)'에 대해 들어 보셨나요? 기후위기를 늦추기 위해선 우선 불필요한 쓰레기를 만들지 않고, 이미 생겼다면 여러 번 쓰고, 버릴 땐 분리배출하는 순으로 실천하는 것이 중요해요. 특히 코로나19 이후로 일회용품 사용이 급증해 더 이상 쓰레기를 묻을 곳마저 없어지고 있어요.",
        contentsD: "알뜰살뜰 요정의 주문을 외우면, 배달 음식을 시킬 때 일회용기를 거절하는 옵션에 꼭 체크하게 되어요.",
        instagramLink: "",
        mainImage:SavingB,
        subImage:SavingB_sub ,
        emoj:emo11a,
    },
    
    ]
},
   
];

export default FairyLists;