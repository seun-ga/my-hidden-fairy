import test01 from "../resources/img/v.10/test01.png"
import emo1a from "../resources/img/v.10/emo1a.png"
import emo1b from "../resources/img/v.10/emo1b.png"
import test02 from "../resources/img/v.10/test02.png"
import emo2a from "../resources/img/v.10/emo2a.png"
import emo2b from "../resources/img/v.10/emo2b.png"
import test03 from "../resources/img/v.10/test03.png"
import emo3a from "../resources/img/v.10/emo3a.png"
import emo3b from "../resources/img/v.10/emo3b.png"
import test04 from "../resources/img/v.10/test04.png"
import emo4a from "../resources/img/v.10/emo4a.png"
import emo4b from "../resources/img/v.10/emo4b.png"
import test05 from "../resources/img/v.10/test05.png"
import emo5a from "../resources/img/v.10/emo5a.png"
import emo5b from "../resources/img/v.10/emo5b.png"
import test06 from "../resources/img/v.10/test06.png"
import emo6a from "../resources/img/v.10/emo6a.png"
import emo6b from "../resources/img/v.10/emo6b.png"
import test07 from "../resources/img/v.10/test07.png"
import emo7a from "../resources/img/v.10/emo7a.png"
import emo7b from "../resources/img/v.10/emo7b.png"
import test08 from "../resources/img/v.10/test08.png"
import emo8a from "../resources/img/v.10/emo8a.png"
import emo8b from "../resources/img/v.10/emo8b.png"
import test09 from "../resources/img/v.10/test09.png"
import emo9a from "../resources/img/v.10/emo9a.png"
import emo9b from "../resources/img/v.10/emo9b.png"
import test10 from "../resources/img/v.10/test10.png"
import emo10a from "../resources/img/v.10/emo10a.png"
import emo10b from "../resources/img/v.10/emo10b.png"
import test11 from "../resources/img/v.10/test11.png"
import emo11a from "../resources/img/v.10/emo11a.png"
import emo11b from "../resources/img/v.10/emo11b.png"
import test12 from "../resources/img/v.10/test12.png"
import emo12a from "../resources/img/v.10/emo12a.png"
import emo12b from "../resources/img/v.10/emo12b.png"


const ProblemSets=[
    {
        key:"1",
        order:"1",
        problemText:"거하게 회식한 다음 날,\n어제 너무 많이 먹은 것 같아. \n오늘의 첫 끼는 어떻게 할까?",
        image:test01,
        ProblemList:[
            {key:"1",
            emoj:emo1a,  
            solutionText: "어제 메뉴가 너무 자극적이었어.시원한 과일 스무디나 만들어먹자.",
            Energy:0,
            Ecology:0,
            Food:4.2,
            Saving:0,},
            {key:"2",
            emoj:emo1b,
            solutionText:"해장국 배달시켜야겠다. 아, 일회용 수저는 빼달라고 부탁해야지!",
            Energy:0,
            Ecology:0,
            Food:0,
            Saving:4.3,
            }
        ]
    },
    {
        key:"2",
        order:"2",
        problemText:"우리 시의 새로운 시장을 \n뽑는 선거기간이 시작되었다. \n나는 어떤 후보에 한 표를 던져볼까?",
        image:test02,
        ProblemList:[
            {key:"1",
            emoj:emo2a,  
            solutionText: "숲을 깎아 주택단지를 만드는 재개발 사업에 반대하는 후보",
            Energy:0,
            Ecology:4.1,
            Food:0,
            Saving:0,},
            {key:"2",
            emoj:emo2b,
            solutionText:"공공 자전거 보급을 확대한다는 공약을 내세우는 후보",
            Energy:4.5,
            Ecology:0,
            Food:0,
            Saving:0,
            }
        ]
    },
    {
        key:"3",
        order:"3",
        problemText:"늘 입던 청바지가 안 맞는다.\n오늘부터 다이어트 돌입! \n작심삼일이 아니어야 할텐데...",
        image:test03,
        ProblemList:[
            {key:"1",
            emoj:emo3a,  
            solutionText: "“헬멧이 어딨더라?”오늘부터 자전거 타고 출퇴근",
            Energy:3.2,
            Ecology:0,
            Food:0,
            Saving:0,},
            {key:"2",
            emoj:emo3b,
            solutionText:" “아... 도시락 유튜브 봐야겠다...”간단한 도시락 직접 싸 다니기",
            Energy:0,
            Ecology:0,
            Food:3,
            Saving:0,
            }
        ]
    },
    {
        key:"4",
        order:"4",
        problemText:"평소 관심 있던 사람이 \n우리 집에 놀러왔다...\n떨린다... 나의 필살기는?",
        image:test04,
        ProblemList:[
            {key:"1",
            emoj:emo4a,  
            solutionText: "나만의 비장의 무기! 맛있는 요리를 해주기 위해 미리 장을 본다",
            Energy:0,
            Ecology:0,
            Food:3.1,
            Saving:0,},
            {key:"2",
            emoj:emo4b,
            solutionText:"로맨틱한 분위기를 내봐야겠어...! 슬그머니 스탠드의 조도를 어둡게 낮춘다",
            Energy:3.2,
            Ecology:0,
            Food:0,
            Saving:0,
            }
        ]
    },
    {
        key:"5",
        order:"5",
        problemText:"나의 첫 자취집 구하기 프로젝트! \n내 마음에 쏙 드는 집을 찾았다. \n거기에 이런 점까지 갖췄다고?",
        image:test05,
        ProblemList:[
            {key:"1",
            emoj:emo5a,  
            solutionText: "산책하기 좋은 공원과 뒷산이 있는 동네의 약간 오래된 집",
            Energy:0,
            Ecology:3.1,
            Food:0,
            Saving:0,},
            {key:"2",
            emoj:emo5b,
            solutionText:"에너지 효율이 좋아서 전기세, 수도세 등 모든 공과금이 적게 나오는 신축 집",
            Energy:4.2,
            Ecology:0,
            Food:0,
            Saving:0,
            }
        ]
    },
    {
        key:"6",
        order:"6",
        problemText:"로션을 사러 올리브형에 온 나.\n판매 1위의 브랜드의 로션은 어떨까? \n앗, 근데 이 점이 마음에 걸린다.",
        image:test06,
        ProblemList:[
            {key:"1",
            emoj:emo6a,  
            solutionText: "동물 실험을 하는 브랜드인 것 같아... 정확하게 확인해봐야겠어.",
            Energy:0,
            Ecology:4.1,
            Food:0,
            Saving:1,},
            {key:"2",
            emoj:emo6b,
            solutionText:"포장재가 너무많은데? 플라스틱을 덜 써서 포장할 순 없었나?",
            Energy:0,
            Ecology:0,
            Food:0,
            Saving:4,
            }
        ]
    },
    {
        key:"7",
        order:"7",
        problemText:"코로나로 \n바깥에 나가기 어려운 요즘, \n나에게 새로운 취미가 생겼다. ",
        image:test07,
        ProblemList:[
            {key:"1",
            emoj:emo7a,  
            solutionText: "‘상추라도 키워볼까?’ 내 방에 작은 텃밭 가꾸기.",
            Energy:0,
            Ecology:0,
            Food:4.4,
            Saving:0,},
            {key:"2",
            emoj:emo7b,
            solutionText:"‘옷장에 잠들어있던 셔츠가 눈에 띄는군' 안 입던 옷 리폼해보기",
            Energy:0,
            Ecology:0,
            Food:0,
            Saving:4.2,
            }
        ]
    },
    {
        key:"8",
        order:"8",
        problemText:"저녁으로 태국음식을 해먹으려 한다.\n하지만 집 주변에서는 바로\n구할 수 없는 레시피라면?",
        image:test08,
        ProblemList:[
            {key:"1",
            emoj:emo8a,  
            solutionText: "귀찮지만 완벽한 요리를 위해 버스타고 장보러 가기",
            Energy:4.4,
            Ecology:0,
            Food:0,
            Saving:0,},
            {key:"2",
            emoj:emo8b,
            solutionText:"맛이 2% 부족해도 괜찮아, 내가 주변에서 살 수 있는 재료로만 요리하기",
            Energy:0,
            Ecology:3.2,
            Food:2.1,
            Saving:0,
            }
        ]
    },
    {
        key:"9",
        order:"9",
        problemText:"오늘은 드디어 대청소 날!\n내가 가장 먼저 정리하고 싶은 것들은?",
        image:test09,
        ProblemList:[
            {key:"1",
            emoj:emo9a,  
            solutionText: "충동구매했지만 잘 쓰지 않은 물건이 너무 많아! 당근상회에 나눔",
            Energy:0,
            Ecology:0,
            Food:0,
            Saving:4.4,},
            {key:"2",
            emoj:emo9b,
            solutionText:"어지럽게 꼬여있는 전선과 콘센트가 너무 많아! 개별 스위치로 정리",
            Energy:4.1,
            Ecology:0,
            Food:0,
            Saving:0,
            }
        ]
    },
    {
        key:"10",
        order:"10",
        problemText:"요새 통잔 잔고가 소박하다.\n하루 1만 원으로 살기 챌린지 도전!\n어떤 것부터 해볼까?",
        image:test10,
        ProblemList:[
            {key:"1",
            emoj:emo10a,  
            solutionText: "동네 전통시장에 가 상품성은 조금 떨어지지만 값은 싼 식재료로 장보기",
            Energy:0,
            Ecology:4.3,
            Food:0,
            Saving:0,},
            {key:"2",
            emoj:emo10b,
            solutionText:"매번 사먹던 생수값도 만만치 않다. 당분간은 수돗물을 받아서 끓여먹기.",
            Energy:0,
            Ecology:0,
            Food:0,
            Saving:3.2,
            }
        ]
    },
    {
        key:"11",
        order:"11",
        problemText:"친구들을 모아 홈파티를 열려고 한다.\n호스트인 내가 손님들에게 \n공지한 파티의 테마는?",
        image:test11,
        ProblemList:[
            {key:"1",
            emoj:emo11a,  
            solutionText: "파티의 끝은 언제나 너무 많은 쓰레기..! 오늘만은 줄여보자, 제로웨이스트 파티",
            Energy:0,
            Ecology:3.1,
            Food:2.2,
            Saving:0,},
            {key:"2",
            emoj:emo11b,
            solutionText:"“재료는 내가 살게, 요리는 누가할래?” 우리집 냉장고파먹기 파티",
            Energy:0,
            Ecology:0,
            Food:4.1,
            Saving:0,
            }
        ]
    },
    {
        key:"12",
        order:"12",
        problemText:"오랜만에 조카를 만나러가는 길에\n도착한 문자 한 통.\n“올 때 맛있는거 사와!”",
        image:test12,
        ProblemList:[
            {key:"1",
            emoj:emo12a,  
            solutionText: "순 식물성 원료로 만든 순한 아이스크림?",
            Energy:0,
            Ecology:0,
            Food:3.2,
            Saving:0,},
            {key:"2",
            emoj:emo12b,
            solutionText:"우리 지역의 곡물로 만든 건강한 초코 쿠키?",
            Energy:0,
            Ecology:3.3,
            Food:0,
            Saving:0,
            }
        ]
    },
    
];

export default ProblemSets;