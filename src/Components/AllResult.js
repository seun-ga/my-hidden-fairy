import React, { Component } from "react";
import FairyLists from "./FairyLists"
import helloImage from "../resources/img/v.10/helloImage.png"
import insta from "../resources/img/v.10/icon-IG.png"
import copyLink from "../resources/img/v.10/icon-Share-right.png"
import tissue from "../resources/img/v.10/icon-tissue.png"
import {CopyToClipboard} from 'react-copy-to-clipboard';
import logo from "../resources/img/v.10/logo_tissue_gca.png"


const RenderFairy=(props)=>{
const {fairy}= props;

return(
    fairy.isAvailable&&
    <div key={fairy.name}>
    <p>{fairy.name}</p>
<div className="allResultWrapper">
    <div className="allResultFairyImg">
        <img src={fairy.mainImage} alt="fairyMainImage"/>
    </div>
    
   <div className="allResult-content">
     {fairy.contentsB}
     {fairy.contentsC}
     </div>
            <div className="basicButton2">
                    <tr>
                        <th><div className="buttonIcon"><img src={helloImage} alt="helloImage"/></div></th>
                        <th><a  id="longTextButton2" href={fairy.instagramLink} style={{cursor: 'pointer'}} rel="noopener noreferrer" target="_blank" >
                        인스타그램 AR필터로 <br></br> 이 요정 만나보기</a></th>
                    </tr>
            </div>
            </div>
 </div>


);

}


class AllResult extends Component{
   
    state={
        isLinkCopied:false,
    }
    
    render(){
       
        return(
        <div className="allResultWrapper">     
            <div className="allResultTitle">
            <p>뾰로롱!<br></br>숨은 기후위기 요정 친구들을 소개합니다</p>
            <div className="allResultSub">숨은 요정 유형 모아보기</div>
            </div>
            {FairyLists.map((fairyList)=>(
            fairyList.isActiveType&&<div key={fairyList.type}>
                <hr id="dashHr"></hr>
                <p id="allResultType">{fairyList.type_k} 타입</p>
                <div className="allResultCont">{fairyList.contentsA}</div>     




                {fairyList.Fairys.map((fairy)=>(
                   <RenderFairy fairy={fairy}></RenderFairy>
                   //이 위의 RenderFairy 안에 Div 존재
                ))}
                
            </div>
            ))}
             <hr id="dashHr"></hr>
                    <div className="shareThing">
                        <tr>
                            <th>
                                <div className="openInsta">
                                    <a className="openInsta" href="https://urlgeni.us/instagram/tissueoffice_" target="blank"><img src={insta} alt="share_instagram"/> <br></br>
                                    <a className="thing">인스타그램에<br></br>공유하기</a> </a>
                                </div>
                            </th>
                            <th className="tableSet"></th>
                            <th>
                                <div >
                                    <CopyToClipboard text="http://findyourfairy.com/#/"
                                            onCopy={() => this.setState({isLinkCopied: true})}>
                                                <div className="copyLink"  >
                                                <img src={copyLink}></img> <br></br>
                                            {this.state.isLinkCopied ? <a className="thing">링크<br></br>복사완료!</a>  : <a className="thing">친구에게<br></br>링크 공유하기</a>}
                                            </div>
                                    </CopyToClipboard>
                                
                                </div>
                            </th>
                        </tr>
                    </div>
                    <br></br>
                    <div className="basicButton4" >
                        <tr>
                            <th><div className="buttonIcon"><img src={tissue} alt="tissueIcon"/></div></th>
                            <th><a id="longTextButton3" href="https://www.instagram.com/tissueoffice/" style={{cursor: 'pointer'}} rel="noopener noreferrer" target="_blank" >티슈 오피스<br></br>인스타그램 구경하기</a> </th>
                        </tr>
                    </div> 
                    <img className = "logo" src={logo} alt="logo"/>
        </div>

        );
    }
    
}
export default AllResult;