import React, { Component } from "react";
import {Link} from "react-router-dom";
import loadingImg from "../resources/img/v.10/finding.gif"
import FairyLists from "./FairyLists"
import store from "../Components/store"


var ScoreSets;


const FindFairy=(props)=> {

    const {fairyList, firstFairyType, secondFairyType_k, scoreDifference, isLoading,percent1 ,percent2}= props;
    const showFairy=firstFairyType===fairyList.type;
    const fairyIndex=scoreDifference>2? 0:1;

    showFairy&&!isLoading&&store.dispatch({type:'FAIRYINFO',
    firstFairyType:fairyList.type,
    firstFairyType_k:fairyList.type_k,
    secondFairyType_k:secondFairyType_k,
    contentsA:fairyList.contentsA,
    selectedFairy:fairyList.Fairys[fairyIndex].isAvailable? fairyList.Fairys[fairyIndex]: fairyList.Fairys[1-fairyIndex],
    friendFairy:fairyList.Fairys[1-fairyIndex],
    percent1 : percent1,
    percent2 : percent2,

    });

    return (
        showFairy&&!isLoading&&
        <Link to={{
            pathname:"/Home/Result", 
        }}>
                <div className="basicButton">찾았다! </div>
                
         </Link>
    );
}



class Loading extends Component{
   
    constructor(props) {

        super(props);
        
        this.state = {
           firstFairyType:store.getState().firstFairyType,
           secondFairyType:store.getState().secondFairyType,
           scoreDifference:0,
           isLoading:true,
           percent1:store.getState().percent1,
           percent2:store.getState().percent2,
           playerName:store.getState().playerName,
       };
 
       const { history} =this.props;

        if(this.state.playerName===""){
            history.push("/");
        }

     }

     componentDidMount(){
        ScoreSets=store.getState().ScoreSets;
        
        //inactive 된 타입은 강제로 점수를 0으로 만듬
        ScoreSets.map((scoreSet,index)=>{
            if(!FairyLists[index].isActiveType){
                scoreSet.Score=0;
            }
            return;
        });

        ScoreSets.sort((a,b) => a.Score < b.Score ? 1 : -1);     
        this.CalculateResult();
        
        setTimeout(()=>{
            this.setState({isLoading:false});
        },4000);
    }

    componentWillUnmount(){
        this.setState({isLoading:true});
    }
    
     CalculateResult=()=>{
        store.dispatch({type:'SCORESETS', ScoreSets:ScoreSets});
       
        this.setState({
           scoreDifference:ScoreSets[0].Score-ScoreSets[1].Score,
           firstFairyType:ScoreSets[0].type,
           secondFairyType_k:FairyLists[ScoreSets[1].index].type_k,
           percent1: (ScoreSets[0].Score/(ScoreSets[0].Score+ScoreSets[1].Score+ScoreSets[2].Score+ScoreSets[3].Score))*100,
           percent2: (ScoreSets[1].Score/(ScoreSets[0].Score+ScoreSets[1].Score+ScoreSets[2].Score+ScoreSets[3].Score))*100,

        });
    }
    
  

    render(){
           
            return(
            <div className="test-background">
                <div className="wrapper">
                    <div className="title">
                    
                    </div> 
                    <div className="loadingImg"><img src={loadingImg} alt="loading-image"/></div>
                    <p>{this.state.playerName} 님 안에 숨어있던 기후위기 요정 찾는중 ...</p>
        
                    
                    {FairyLists.map((fairyList)=>(
                        <FindFairy key={fairyList.type} fairyList={fairyList} playerName={this.state.playerName}
                        firstFairyType={this.state.firstFairyType} scoreDifference={this.state.scoreDifference} isLoading={this.state.isLoading}
                        secondFairyType_k={this.state.secondFairyType_k}
                        percent1={this.state.percent1} percent2={this.state.percent2}>
                        </FindFairy>
                    ))}
        
                </div>
            </div>
            );
       
    }
    
}
export default Loading;