 import {createStore} from 'redux';

 export default createStore(function(state, action){
    if(state ===undefined){
        return {
            playerName:"",

            firstFairyType:"",
            
            firstFairyType_k:"",
            secondFairyType_k:"",
            contentsA:"",
            selectedFairy:"",
            friendFairy:"",
            percent1 : 0,
            percent2 : 0,

            
            ScoreSets: [{type:"Energy", Score:0, index:0},
            {type:"Ecology",Score:0, index:1},
            {type:"Food",Score:0, index:2},
            {type:"Saving",Score:0, index:3}]
    
        }
    }
    
    if(action.type==='NAME'){
        return {...state, playerName:action.playerName}
    }
    if(action.type==='SCORESETS'){
        return {...state, ScoreSets:action.ScoreSets}
    }
    if(action.type==='FAIRYINFO'){
        return {...state, 
            firstFairyType:action.firstFairyType,
            firstFairyType_k:action.firstFairyType_k,
            secondFairyType_k:action.secondFairyType_k,
            contentsA:action.contentsA,
            selectedFairy:action.selectedFairy,
            friendFairy:action.friendFairy,
            percent1 : action.percent1,
            percent2 : action.percent2,
        }
    }

    

    return state;
 }, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())
