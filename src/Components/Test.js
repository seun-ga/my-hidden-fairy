import React, { Component } from "react";
import ProblemSets from "./ProblemSet"
import store from "../Components/store"
import audioClip from "../resources/music/backgroundMusic.m4a"
import back from "../resources/img/v.10/backButtonImage.png"

class Test extends Component{
    constructor(props) {
        super(props);
        
        this.state = {
            problemNum:0,
            Energy:0,
            Ecology:0,
            Food:0,
            Saving:0,
            history:[]
       };
       
     }
    

     componentDidUpdate() {
        const {location, history} =this.props;
        // 전형적인 사용 사례 (props 비교를 잊지 마세요)
       
        if(location.state ===undefined){
            history.push("/Home/NameField");
        }
            console.log("Component did update");
            if(this.state.problemNum>=ProblemSets.length){
                history.push({
                    pathname: './Loading',
                    state: {
                        playerName:location.state.playerName,
                        Energy:this.state.Energy,
                        Ecology:this.state.Ecology,
                        Food:this.state.Food,
                        Saving:this.state.Saving,
                        problemNum:this.state.problemNum
                    }
                });    
               
            }
    
        
      }
     
     GoNextProblem=(problem,Energy,Ecology,Food,Saving)=>{
      
        

        this.setState({
            problemNum:this.state.problemNum+1,
            Energy:this.state.Energy+Energy,
            Ecology:this.state.Ecology+Ecology,
            Food:this.state.Food+Food,
            Saving:this.state.Saving+Saving,
            history: this.state.history.concat(problem),
         });
         
     }


     GoPreviousProblem=()=>{
        const currentHistory=this.state.history;

        if(currentHistory.length===0){
            console.log("You can not go back");
        }else if(currentHistory.length===1){
            this.setState({
                problemNum:this.state.problemNum-1,
                Energy:this.state.Energy-currentHistory[currentHistory.length-1].Energy,
                Ecology:this.state.Ecology-currentHistory[currentHistory.length-1].Ecology,
                Food:this.state.Food-currentHistory[currentHistory.length-1].Food,
                Saving:this.state.Saving-currentHistory[currentHistory.length-1].Saving,
                history:[],
             });
        }else{
            this.setState({
                problemNum:this.state.problemNum-1,
                Energy:this.state.Energy-currentHistory[currentHistory.length-1].Energy,
                Ecology:this.state.Ecology-currentHistory[currentHistory.length-1].Ecology,
                Food:this.state.Food-currentHistory[currentHistory.length-1].Food,
                Saving:this.state.Saving-currentHistory[currentHistory.length-1].Saving,
                history: this.state.history.slice(0, currentHistory.length-1),
             });
        }
         
     }

     
    

    render(){
        console.log(this.state);
       
        if(this.state.problemNum>=ProblemSets.length){
            store.dispatch({type:'SCORESETS',
            ScoreSets:[
                    {type:"Energy", Score:this.state.Energy, index:0},
                    {type:"Ecology",Score:this.state.Ecology, index:1},
                    {type:"Food",Score:this.state.Food, index:2},
                    {type:"Saving",Score:this.state.Saving, index:3}
            ]
             });
            return(<div></div>);
        }

        

        return(
            
            <div className="test-background">
                <div className="testTextWrapper">
                <div className="testText">
                    {
                    ProblemSets[this.state.problemNum].problemText.split("\n").map(function(item, idx) {
                    return (
                     <span className="problemText" key={idx}>
                   {item}
                   <br/>
                    </span>
                            )
                   })}
                </div>
                </div>
                <div className="solution-wrapper">
                
                    <img className="solutionImg" src={ProblemSets[this.state.problemNum].image} alt="solution Image"/>

                </div>

                <div className="button-container">
                        {ProblemSets[this.state.problemNum].ProblemList.map((problem) => (  
                                <div className="basicButton" key={problem.key}
                                    onClick={() => this.GoNextProblem(problem,problem.Energy,problem.Ecology,problem.Food,problem.Saving)}> 
                                        <img src={problem.emoj}></img>
                                </div>
                        ))}
                </div>

                 <div className="Progress">
                        <div className="backButton" onClick={this.GoPreviousProblem}>
                            <img src={back}></img>
                        </div>
                  
                        <div className="ProgressBackground" style={{width:260}} >
                        <div className="ProgressBar" style={{width:((this.state.problemNum+1)/12)*260}}></div>
                     </div>
                     <div className="ProgressCount"><h5> {this.state.problemNum+1}/{ProblemSets.length}</h5></div>

                </div>
                 

                <audio src={audioClip}  autoPlay loop style={{visibility:'hidden'}}/>
                    

            </div>

        );
    }
    
}
export default Test;