import React, { Component } from "react";
import HomeImage from "../resources/img/v.10/HomeImage.png"
import logo from "../resources/img/v.10/logo_tissue_gca.png"
import titleLogo from "../resources/img/v.10/title_logo.png"
import {Link} from "react-router-dom";



class Start extends Component{
   
    state={
        IsNameField:false,
        playerName:"아무개"
    }

   
    render(){
        
        return(
        <div>
            <div className="wrapper" >
            
                <div className="title">
                    <img src={titleLogo} alt="titleLogo"/>
                    <h3>내 안의 기후위기 요정을 찾아서!</h3> 
                </div>

    
                <div>
                    <img id="homeImage" src={HomeImage} alt="HomeImage"/>
                    <div className="button-container">
                        <Link to ="./Home/NameField">
                        <div className="basicButton" >
                            출발!
                        </div>
                        </Link>
                    </div>
                </div>

                <div className="pcAlert" >
                    <h3>이 캠페인은 모바일로만 참여가 가능합니다.🧚‍♀️</h3>
                </div>
           
                <img className = "logo" src={logo} alt="logo"/>
            </div>
        </div>
        );
    }
    
}
export default Start;