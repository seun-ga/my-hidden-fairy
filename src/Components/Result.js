import React, { Component } from "react";
import {Link} from "react-router-dom";
import helloImage from "../resources/img/v.10/helloImage.png"
import wiki from "../resources/img/v.10/icon-wiki.png"
import tissue from "../resources/img/v.10/icon-tissue.png"
import {CopyToClipboard} from 'react-copy-to-clipboard';
import store from "./store";
import logo from "../resources/img/v.10/logo_tissue_gca.png"
import resultAudioClip from "../resources/music/resultMusic_edit.m4a"
import insta from "../resources/img/v.10/icon-IG.png"
import copyLink from "../resources/img/v.10/icon-Share-right.png"



class Result extends Component{
   
    state={
        isHashtagCopied:false,
        isLinkCopied:false,
        playerName:store.getState().playerName,

        firstFairyType:store.getState().firstFairyType,
        
        firstFairyType_k:store.getState().firstFairyType_k,
        secondFairyType_k:store.getState().secondFairyType_k,

        contentsA:store.getState().contentsA,

        selectedFairy:store.getState().selectedFairy,
        friendFairy:store.getState().friendFairy,

        percent1 : store.getState().percent1,
        percent2 : store.getState().percent2,

   }

    render(){
        const { history} =this.props;

        if(this.state.selectedFairy===""){
            history.push("/");
        }
       

        return(
        <div className="wrapper">

            {/* 요정 소개*/}
            <div className="title">
                <h3>뾰로롱! <a>{this.state.playerName}</a> 님 안에
                숨어있던 기후위기 요정은?</h3>
            </div>

            
            <img className="result-data" src={this.state.selectedFairy.mainImage} alt="mainImage"/>
            <p id="subtext">(위 요정 이미지를 꾹 눌러 사진첩에 저장하세요)</p>

             {/* 분야별 퍼센트 */}
            <div className="percent">
                <p className="percentText"> 나의 기후위기 타입은?</p>
                <div className="percentGraph">
                    <div className="percent1" style={{height:this.state.percent1*2}}>
                        <div className="percent1st"> <a>1st: </a>{this.state.firstFairyType_k} </div>
                    </div>
                   
                    <div className="percent2" style={{height:this.state.percent2*2}}>
                        <div className="percent2nd"> <a>2nd: </a>{this.state.secondFairyType_k} </div>
                    </div>
                </div>

            </div>
        
            
            {/* 당신은 ~ 요정 */}
            <div className="subFairy">
            <img src= {this.state.selectedFairy.subImage} alt="subImage"/>
            </div>
        
            <div className="mainContents">
                <div className="firstBox">
                <p id="sub1">{this.state.playerName}님은 {this.state.selectedFairy.name}!</p>
                <p className="contentsA">{this.state.contentsA}</p>
                <div>
                    {this.state.selectedFairy.contentsB.split("\n").map(function(item, idx) {
                    return (
                     <span className="contentsB" key={idx}>
                   {item}
                   <br/>
                    </span>
                )})}
                </div>
                

                </div>

            </div>
            <div className="basicButton2">
                    <tr>
                        <th><div className="buttonIcon"><img src={helloImage} alt="helloImage"/></div></th>
                        <th><a  id="longTextButton2" href={this.state.selectedFairy.instagramLink} style={{cursor: 'pointer'}} rel="noopener noreferrer" target="_blank" >
                        인스타그램 AR필터로 <br></br> 내 요정 만나보기</a></th>
                    </tr>
            </div>  

              {/* 기후위기 주문의 효과 */}
            <div className="resultEffect">
                    기후위기 주문의 효과는?
            <br/>
            <div className="contentsC" > {this.state.selectedFairy.contentsC}</div>
            <div className="EffectEmoj">
                    <img src= {this.state.selectedFairy.emoj} alt="emoj"/>
                </div>
            <div className="contentsD" > {this.state.selectedFairy.contentsD}</div>
                
                
            </div>
            
            {/* 친구 요정 추천 */}

            {/* <div className="frinedFairy">
                <p id="sub2">친구하기 좋은 요정은?</p>
                <img src={this.state.friendFairy.subImage}/> <br></br>
                <p className="contentsA">
                    안녕! 저는 {this.state.firstFairyType_k} 타입의 {this.state.friendFairy.name} 이에요. 저랑 친구를 한다면 분명 잘 어울릴 것 같은 예감이 들어요! 
                </p>
        </div>   */}
            
            
            {/* 버튼 모음 */}

            <Link to ="/Home/AllResult">
                <div className="basicButton3">
                    <tr>
                        <th><div className="buttonIcon"><img src={wiki} alt="wikiIcon"/></div></th>
                        <th><div id="longTextButton2">숨은 요정 유형 <br></br> 모두 모아보기</div></th>
                    </tr>
                </div>
            </Link>

            <div className="basicButton4" >
                <tr>
                    <th><div className="buttonIcon"><img src={tissue} alt="tissueIcon"/></div></th>
                    <th><a id="longTextButton3" href="https://www.instagram.com/tissueoffice/" style={{cursor: 'pointer'}} rel="noopener noreferrer" target="_blank" >티슈 오피스<br></br>인스타그램 구경하기</a> </th>
                </tr>
            </div> 

            <hr id="dashHr"></hr>

            <div className="hashtagText">
                {this.state.selectedFairy.hashtag}
            </div>

            <div className="hashtagCopy">

                <CopyToClipboard text={this.state.selectedFairy.hashtag}
                                onCopy={() => this.setState({isHashtagCopied: true})}>
                                    <div className="Button" id="hashtag" >
                                {this.state.isHashtagCopied ? <span >복사 완료!</span> : <span>나의 요정 소개 복사하기</span>}
                                </div>
                </CopyToClipboard>
            </div>
        <div className="shareThing">
                <tr>
                    <th>
                        <div className="openInsta">
                            <a className="openInsta" href="https://urlgeni.us/instagram/tissueoffice_" target="blank"><img src={insta}></img> <br></br>
                            <a className="thing">인스타그램에<br></br>공유하기</a> </a>
                        </div>
                    </th>
                    <th className="tableSet"></th>
                    <th>
                        <div >
                            <CopyToClipboard text="http://findyourfairy.com/#/"
                                    onCopy={() => this.setState({isLinkCopied: true})}>
                                        <div className="copyLink"  >
                                        <img src={copyLink}></img> <br></br>
                                    {this.state.isLinkCopied ? <a className="thing">링크<br></br>복사완료!</a>  : <a className="thing">친구에게<br></br>링크 공유하기</a>}
                                    </div>
                            </CopyToClipboard>
                        
                        </div>
                    </th>
                </tr>
        </div>
            <hr id="dashHr"></hr>
            
            {/* 티슈오피스 소개(추가) */}
            <div className="proInfo">
                기획 및 개발: 티슈 오피스 <br></br>
                일러스트레이션: OOO <br></br><br></br>
                〈숨은 요정 찾기: 내 안의 기후위기 요정을 찾아서〉는 경기도콘텐츠진흥원의 ‘문화기술 공공 콘텐츠 제작지원 사업’에 선정되어 제작된 기후위기 대응 모바일 캠페인입니다. <br></br>
                <div className="centerImg"><img src={logo} alt="logo"/></div>
            </div>
           

            <audio src={resultAudioClip} autoPlay style={{visibility:'hidden'}}/>
            
        </div>

        );
    }
    
}
export default Result;