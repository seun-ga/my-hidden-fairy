import React, { Component } from "react";
import {Link} from "react-router-dom";
import helloImage from "../resources/img/v.10/helloImage.png"
import store from "../Components/store"


class Hello extends Component{

    state = {
        playerName:store.getState().playerName,
      }
  
    constructor(props){
        super(props);
        const { history} =this.props;

        if(this.state.playerName===""){
            history.push("/");
        }
      }
    
    
    render(){
        
        
        
            return(
            
                <div className="wrapper">
        
                    <h3 className="bigText">반가워요, {this.state.playerName} 님!</h3>
                    <div id="helloImage"><img src={helloImage} alt="helloImage"/></div>
                    <div className="firstBalloon">
                        <p>〈숨은 요정 찾기: 내 안의 기후위기 요정을 찾아서!〉는 일상 속 기후위기 대응 액션에 대해 알려주는 캠페인이에요.</p>
                    </div>
                    <div className="secBalloon">
                        <p>이제부터 나오는 질문들에 답해주시면 {this.state.playerName} 님과 가장 잘 어울리는 기후위기 요정을 찾아드릴게요!</p>
                    </div>
                    <div className="button-container">
        
                        <Link to={{
                                pathname:"/Home/Test", 
                                state:{
                                    playerName: this.state.playerName
                                }
                                }}>
                            <div className="basicButton">좋아요!</div>
                        </Link>
                    </div>
                   
                </div>
                );
       
        
    }
    
}
export default Hello;