import React, { Component } from "react";
import {Link} from "react-router-dom";
import NameRegisterImg from "../resources/img/v.10/NameRegister.png"
import store from "../Components/store"

class NameField extends Component{

    state = {
      playerName:store.getState().playerName,
    }


    handleChange = (e) => {
      store.dispatch({type:'NAME', playerName:e.target.value});
        this.setState({
          playerName: e.target.value
        });
        
      }
    

    render(){
        return(
          <div className="wrapper">
          <div className="nameTextWrapper">
           <div className="nameText"> 
           <span>당신을 뭐라고 부르면 좋을까요?</span>
           </div>
           </div>
          <div className="nameImage">
            <img src={NameRegisterImg} alt="HomeImage"/>
          </div>
            <br></br>
            <form onSubmit={this.handleSubmit}>
           <div className="namePlaceHolder">
            <input
              placeholder="1-8자 입력"
              value={this.state.playerName}
              type="text"
              onChange={this.handleChange}
              maxLength="8"
              name="name"
            />
            </div>
          

            
            {this.state.playerName===""||this.state.playerName===" "?
            <div id="nameButtonCenter"><button disabled>확인</button></div>:
            
            <Link to="/Home/Hello">
                <div id="nameButtonCenter">
                  <button type="submit">확인</button>
                </div>
            </Link>}
        
           
          </form> 
          </div>   
        );
    }
}
    
export default NameField;