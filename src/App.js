import React from 'react';
import './App.css';
import {HashRouter, Route} from "react-router-dom";
import Start from "./Components/Start"
import NameField from "./Components/NameField"
import Test from "./Components/Test"
import Result from "./Components/Result"
import AllResult from "./Components/AllResult"
import Loading from "./Components/Loading"
import Hello from "./Components/Hello"


function App(){
  
  return(

    <HashRouter> 
      
      <Route path="/" exact={true} component={Start}>  
      </Route>
      <Route parth="/Home">
        
      </Route>
      <Route path="/Home/Start" component={Start}/>
      <Route path="/Home/Hello" component={Hello}/>
      <Route path="/Home/Test" component={Test}/>
      
      <Route path="/Home/Result" component={Result}/>
      <Route path="/Home/NameField" component={NameField}/>
      <Route path="/Home/AllResult" component={AllResult}/>
      <Route path="/Home/Loading" component={Loading}/>
    
    </HashRouter>
  );
}

export default App;
